using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Question", menuName = "ScriptableObjects/QuestionSO", order = 1)]
public class QuestionSO : ScriptableObject
{
    public string SOQuestionText;

    public string SOAnswerTextA;
    public string SOAnswerTextB;
    public string SOAnswerTextC;
    public string SOAnswerTextD;
    public string SOAnswerTextE;

    public string SOCorrectAnswer;
    public string SORecordedAnswer;
}
