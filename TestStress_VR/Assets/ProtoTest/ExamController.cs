using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ExamController : MonoBehaviour
{
    //Text fields displayed to the user - Data is taken from the Question Scriptable Objects
    public Text QuestionText;

    public Text AnswerAText;
    public Text AnswerBText;
    public Text AnswerCText;
    public Text AnswerDText;
    public Text AnswerEText;

    //Scriptable Object Arrays
    public QuestionSO[] EnglishQuestions;
    public QuestionSO[] MathQuestions;
    public QuestionSO[] EnglishStressors;
    public QuestionSO[] MathStressors;

    //The final array of questions displayed on the Exam - populated from the arrays above
    public ScriptableObject[] CurrentExam;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void SetTest(int questionNum)
    {

    }
}
